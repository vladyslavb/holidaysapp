 //
//  MainDataDisplayManager.h
//  UAHolidaysApp
//
//  Created by Vladyslav Bedro on 8/15/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

//frameworks
#import <Foundation/Foundation.h>
#import "UIKit/UIKit.h"

//protocols
#import "DataDisplayManagerOutput.h"

@interface MainDataDisplayManager : NSObject <UITableViewDataSource, UITableViewDelegate>

//methods

/**
 @author Vladyslav Bedro
 
 Method initializes and binds components of the MVC pattern
 */
- (instancetype) initWithOutput: (id<DataDisplayManagerOutput>) output;

/**
 @author Vladyslav Bedro
 
 Method fills delegate of mainTableViewController with all holidays
 */
- (void) fillWithHolidays: (NSMutableArray*) holidays;

@end
