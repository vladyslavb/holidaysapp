//
//  MainDataDisplayManager.m
//  UAHolidaysApp
//
//  Created by Vladyslav Bedro on 8/15/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "MainDataDisplayManager.h"

//classes
#import "HolidayCell.h"
#import "Holiday+CoreDataClass.h"

#import "AddEditViewController.h"

//static variables
static NSString* kHollidayIdentifier = @"HolidayCell_ID";

@interface MainDataDisplayManager()

//properties
@property (weak, nonatomic) id<DataDisplayManagerOutput> output;

@property (strong, nonatomic) NSMutableArray* holidaysArray;

@end

@implementation MainDataDisplayManager


#pragma mark - Initialization -

- (instancetype) initWithOutput: (id<DataDisplayManagerOutput>) output
{
    if (self == [super init])
    {
        self.output = output;
    }
    
    return self;
}

- (NSMutableArray*) holidaysArray
{
    if (_holidaysArray == nil)
    {
        _holidaysArray = [self.output getAllHolidays];
    }
    
    return _holidaysArray;
}


#pragma mark - UITableViewDataSource methods -

- (NSInteger) numberOfSectionsInTableView: (UITableView*) tableView
{
    return 1;
}

- (NSInteger) tableView: (UITableView*) tableView
  numberOfRowsInSection: (NSInteger)    section
{
    return self.holidaysArray.count;
}

- (UITableViewCell*) tableView: (UITableView*) tableView
         cellForRowAtIndexPath: (NSIndexPath*) indexPath
{
    HolidayCell* cell = [tableView dequeueReusableCellWithIdentifier: kHollidayIdentifier];

    
    Holiday* holiday = self.holidaysArray[indexPath.row];
    
    [cell fillContentWithHoliday: holiday];
    
    return cell;
}

- (void) tableView: (UITableView*)                tableView
commitEditingStyle: (UITableViewCellEditingStyle) editingStyle
 forRowAtIndexPath: (NSIndexPath*)                indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        Holiday* holiday = self.holidaysArray[indexPath.row];
        
        [holiday MR_deleteEntity];
        
        [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
        
        [self.holidaysArray removeObjectAtIndex: indexPath.row];
        
        [tableView deleteRowsAtIndexPaths: [NSArray arrayWithObject: indexPath]
                         withRowAnimation: UITableViewRowAnimationFade];
    }
}


#pragma mark - UITableViewDelegate methods -

- (BOOL)                     tableView: (UITableView*) tableView
shouldIndentWhileEditingRowAtIndexPath: (NSIndexPath*) indexPath
{
    return NO;
}

- (void)              tableView: (UITableView*) tableView
        didSelectRowAtIndexPath: (NSIndexPath*) indexPath
{
    if ([self.output respondsToSelector: @selector(didSelectItemWithHoliday:)])
    {
        Holiday* selectedHoliday = self.holidaysArray[indexPath.row];
        
        [self.output didSelectItemWithHoliday: selectedHoliday];
    }
    
    [tableView deselectRowAtIndexPath: indexPath
                             animated: YES];
}

- (BOOL)               tableView: (UITableView*) tableView
 shouldShowMenuForRowAtIndexPath: (NSIndexPath*) indexPath
{
    return YES;
}


#pragma mark - Public methods -

- (void) fillWithHolidays: (NSMutableArray*) holidays
{
    self.holidaysArray = holidays;
}

@end





















