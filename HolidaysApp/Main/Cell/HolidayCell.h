//
//  HolidayCell.h
//  UAHolidaysApp
//
//  Created by Vladyslav Bedro on 8/15/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Holiday;

@interface HolidayCell : UITableViewCell

/**
 @author Vladyslav Bedro
 
 Method fill cell with person information
 */
- (void) fillContentWithHoliday: (Holiday*) holiday; //will be done

@end
