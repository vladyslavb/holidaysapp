//
//  HolidayCell.m
//  UAHolidaysApp
//
//  Created by Vladyslav Bedro on 8/15/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "HolidayCell.h"

#import "Holiday+CoreDataClass.h"

@interface HolidayCell ()

//outlets
@property (weak, nonatomic) IBOutlet UILabel* nameHolidayLabel;
@property (weak, nonatomic) IBOutlet UILabel* dateHolidayLabel;

@end

@implementation HolidayCell


#pragma mark - Default cell methods -

- (void) awakeFromNib
{
    [super awakeFromNib];
}

- (void) setSelected: (BOOL) selected
            animated: (BOOL) animated
{
    [super setSelected: selected
              animated: animated];
}


#pragma mark - Public methods -

- (void) fillContentWithHoliday: (Holiday*) holiday
{
    self.nameHolidayLabel.text = holiday.name;
    self.dateHolidayLabel.text = holiday.date;
}

@end
