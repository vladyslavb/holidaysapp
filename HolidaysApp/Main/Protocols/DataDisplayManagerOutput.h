//
//  DataDisplayManagerOutput.h
//  UAHolidaysApp
//
//  Created by Vladyslav Bedro on 8/15/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Holiday;

@protocol DataDisplayManagerOutput <NSObject>

//methods

/**
 @author Vladyslav Bedro
 
 Method make push and moving to information controller with holiday information
 */
- (void) didSelectItemWithHoliday: (Holiday*) holiday;

/**
 @author Vladyslav Bedro
 
 Method gets all of holidays
 */
- (NSMutableArray*) getAllHolidays;

@end
