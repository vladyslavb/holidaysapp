//
//  MainTableViewController.m
//  UAHolidaysApp
//
//  Created by Vladyslav Bedro on 8/15/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "MainTableViewController.h"

// protocols
#import "MainDataDisplayManager.h"

//classes
#import "Holiday+CoreDataClass.h"
#import "AddEditViewController.h"

//static variables
static NSString* const BaseURLString = @"https://holidayapi.com";
static NSString* const apiKey        = @"bc67fd25-1d6f-4af5-8d18-48c1e953dabb"; // my API_key from site

@interface MainTableViewController ()

//outlets
@property (weak, nonatomic) IBOutlet UITableView*     mainTableView;
@property (weak, nonatomic) IBOutlet UIBarButtonItem* editButton;

//properties
@property (strong, nonatomic) MainDataDisplayManager* displayManager;

@property (strong, nonatomic) NSMutableArray* holidaysArray;
@property (strong, nonatomic) NSDictionary*   allResponse;

@end

@implementation MainTableViewController

#pragma mark - Life cycle -

- (void) viewDidLoad
{
    [super viewDidLoad];
    
    [self fetchHolidays];
    
    [self.displayManager fillWithHolidays: self.holidaysArray];
    
    [self bindingUI];
}

- (void) viewWillAppear: (BOOL) animated
{
    [super viewWillAppear: animated];
    
    [self fetchHolidays];

    [self.displayManager fillWithHolidays: self.holidaysArray];
    
    [self.mainTableView reloadData];
}


#pragma mark - Memory managment -

- (void) didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


#pragma mark - Properties -

- (MainDataDisplayManager*) displayManager
{
    if (_displayManager == nil)
    {
        _displayManager = [[MainDataDisplayManager alloc] initWithOutput: self];
    }
    
    return _displayManager;
}


#pragma mark - Display manager output methods -

- (void) didSelectItemWithHoliday: (Holiday*) holiday
{
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName: @"AddEdit"
                                                         bundle: nil];
                        
    AddEditViewController* addEditVC =
            [storyboard instantiateViewControllerWithIdentifier: @"AddEditStoryboardID"];
    
    [addEditVC fillViewWithHoliday: holiday];
    
    [self.navigationController pushViewController: addEditVC
                                         animated: YES];
}

- (NSMutableArray*) getAllHolidays
{
    [self fetchHolidays];
    
    return self.holidaysArray;
}


#pragma mark - Actions -

- (IBAction) onAddHollidayPressed: (UIBarButtonItem*) sender
{
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName: @"AddEdit"
                                                         bundle: nil];
    
    AddEditViewController* addEditVC =
        [storyboard instantiateViewControllerWithIdentifier: @"AddEditStoryboardID"];
    
    [self.navigationController pushViewController: addEditVC
                                         animated: YES];
}

- (IBAction) onEditButtonPressed: (UIBarButtonItem*) sender
{
    [self.mainTableView setEditing: !self.mainTableView.editing
                          animated: YES];
    
    if (self.mainTableView.editing)
    {
        self.editButton.tintColor = [UIColor blackColor];
        self.editButton.title     = @"Done"; //TODO: CHECK
    }
    else
    {
        self.editButton.tintColor = [UIColor blueColor];
        self.editButton.title     = @"Edit"; //TODO: CHECK
    }
}


#pragma mark - UI -

- (void) bindingUI
{
    self.mainTableView.dataSource                   = self.displayManager;
    self.mainTableView.delegate                     = self.displayManager;
    self.mainTableView.editing                      = NO;
    self.mainTableView.allowsSelectionDuringEditing = NO;
    
    [self.mainTableView registerNib: [UINib nibWithNibName: @"HolidayCell"
                                                    bundle: nil]
             forCellReuseIdentifier: @"HolidayCell_ID"];
    
    if (self.holidaysArray.count == 0)
    {
      [self getAllHolidaysForCountry: @"UA"];
    }
    
}


#pragma mark - Internal methods -

-(void) fetchHolidays
{
    self.holidaysArray = [NSMutableArray arrayWithArray: [Holiday MR_findAllSortedBy: @"date"
                                                                           ascending: YES]];
}

-(void) getAllHolidaysForCountry: (NSString*) country
{
    NSDateComponents* components = [[NSCalendar currentCalendar] components: NSCalendarUnitDay |
                                                                             NSCalendarUnitMonth |
                                                                             NSCalendarUnitYear
                                                                   fromDate: [NSDate date]];
    
    NSInteger previousYear  = components.year - 1;
    
    //NSInteger month = [components month] - 3;
    
    // @"%@/v1/holidays?country=%@&year=%ld&month=0%ld&key=%@"
    
    NSString* string =
        [NSString stringWithFormat: @"%@/v1/holidays?country=%@&year=%ld&key=%@",
                                                                                BaseURLString,
                                                                                country,
                                                                                (long) previousYear,
                                                                                /*(long) month,*/
                                                                                apiKey];
    
    NSURL* URL = [NSURL URLWithString: string];
    
    AFHTTPSessionManager* manager = [AFHTTPSessionManager manager];
    
    [manager POST: URL.absoluteString
       parameters: nil
         progress: nil
          success: ^(NSURLSessionTask *task, id responseObject)
    {
        [MagicalRecord saveWithBlock: ^(NSManagedObjectContext* localContext) {
            
            [Holiday MR_truncateAllInContext: localContext];
            
            NSDictionary* holidaysDictionary = responseObject[@"holidays"];
            
            NSArray* tempArray = [holidaysDictionary allValues];
            
            NSMutableArray* tempMuttableArray = [NSMutableArray new];

            for(NSMutableArray* subArray in tempArray)
            {
                for(NSDictionary* tempDictionary in subArray)
                {
                    [tempMuttableArray addObject: tempDictionary];
                }
            }
            
            NSArray* newHolidayArray =
                [Holiday MR_importFromArray: tempMuttableArray
                                  inContext: localContext];
            NSError* error;
            
            if ([localContext obtainPermanentIDsForObjects: newHolidayArray
                                                     error: &error])
            {
                for (NSManagedObject* obj in newHolidayArray)
                {
                    
                    //Holiday* existingEntity =
//                        [Holiday MR_findFirstByAttribute: @"name"
//                                               withValue: allNames
//                                               inContext: localContext];
                    
                    //if (!existingEntity) {
                    if (![[obj objectID] isEqual: [NSNull null]])
                    {
                        [self.holidaysArray addObject: [obj objectID]];
                    }
                    else
                    {
                     NSLog(@"It was null");
                    }
                    //  }
                }
            }
            
        } completion:^(BOOL success, NSError *error) {
            if (!success) {
                NSLog(@"Error: %@",error.localizedDescription);
            }
            else
            {
                [self fetchHolidays];
                
                [self.displayManager fillWithHolidays: self.holidaysArray];
                
                [self.mainTableView reloadData];
                
                NSLog(@"Success %d", success);
            }
            
        }];
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
    
}


@end


























