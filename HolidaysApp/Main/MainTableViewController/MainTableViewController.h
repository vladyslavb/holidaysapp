//
//  MainTableViewController.h
//  UAHolidaysApp
//
//  Created by Vladyslav Bedro on 8/15/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import <UIKit/UIKit.h>

//protocols
#import "DataDisplayManagerOutput.h"

@interface MainTableViewController : UIViewController <DataDisplayManagerOutput>

//properties
@property (weak, nonatomic) id<DataDisplayManagerOutput> output;

@end
