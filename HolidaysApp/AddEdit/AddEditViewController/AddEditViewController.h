//
//  AddEditViewController.h
//  UAHolidaysApp
//
//  Created by Vladyslav Bedro on 8/15/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import <UIKit/UIKit.h>

//classes
#import "Holiday+CoreDataClass.h"

@interface AddEditViewController : UIViewController

//methods

- (void) fillViewWithHoliday: (Holiday*) holiday;

@end
