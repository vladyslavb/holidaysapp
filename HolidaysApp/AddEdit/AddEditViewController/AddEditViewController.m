//
//  AddEditViewController.m
//  UAHolidaysApp
//
//  Created by Vladyslav Bedro on 8/15/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "AddEditViewController.h"

@interface AddEditViewController ()

//outlets
@property (weak, nonatomic) IBOutlet UITextField* nameField;
@property (weak, nonatomic) IBOutlet UITextField* dateField;
@property (weak, nonatomic) IBOutlet UIButton*    addEditButton;

//properties
@property (strong, nonatomic) Holiday* selectedHoliday;

@end

@implementation AddEditViewController


#pragma mark - Life cycle -

- (void) viewDidLoad
{
    [super viewDidLoad];
    
    [self checkConfiguration];
}


#pragma mark - Memory managment -

- (void) didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


#pragma mark - Public methods -

- (void) fillViewWithHoliday: (Holiday*) holiday
{
    self.selectedHoliday = holiday;
}


#pragma mark - Actions -

- (IBAction) onAddButtonPressed: (UIButton*) sender
{
    if (self.selectedHoliday == nil)
    {
        self.selectedHoliday = [Holiday MR_createEntity];
    }
    
    self.selectedHoliday.name = self.nameField.text;
    self.selectedHoliday.date = self.dateField.text;
    
    [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
    
    [self.navigationController popViewControllerAnimated: YES];

}


#pragma mark - Internal methods -

- (void) checkConfiguration
{
    if (self.selectedHoliday)
    {
        [self.nameField setText: [NSString stringWithFormat: @"%@", self.selectedHoliday.name]];
        [self.dateField setText: [NSString stringWithFormat: @"%@", self.selectedHoliday.date]];
        
        [self.addEditButton setTitle: @"Update"
                            forState: UIControlStateNormal];
    }
}

@end






















