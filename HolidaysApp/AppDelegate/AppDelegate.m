//
//  AppDelegate.m
//  HolidaysApp
//
//  Created by Vladyslav Bedro on 8/15/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)          application: (UIApplication*) application
didFinishLaunchingWithOptions: (NSDictionary*)  launchOptions
{
    [MagicalRecord setupCoreDataStack];
    
    return YES;
}


- (void) applicationWillResignActive: (UIApplication*) application
{

}


- (void) applicationDidEnterBackground: (UIApplication*) application
{

}


- (void) applicationWillEnterForeground: (UIApplication*) application
{

}


- (void) applicationDidBecomeActive: (UIApplication*) application
{
    
}


- (void) applicationWillTerminate: (UIApplication*) application
{
    [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
}


@end
