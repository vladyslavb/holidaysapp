//
//  Holiday+CoreDataClass.h
//  HolidaysApp
//
//  Created by Vladyslav Bedro on 8/15/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface Holiday : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "Holiday+CoreDataProperties.h"
