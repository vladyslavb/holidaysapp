//
//  Holiday+CoreDataProperties.m
//  HolidaysApp
//
//  Created by Vladyslav Bedro on 8/15/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//
//

#import "Holiday+CoreDataProperties.h"

@implementation Holiday (CoreDataProperties)

+ (NSFetchRequest<Holiday *> *)fetchRequest {
	return [NSFetchRequest fetchRequestWithEntityName:@"Holiday"];
}

@dynamic date;
@dynamic name;

@end
