//
//  Holiday+CoreDataProperties.h
//  HolidaysApp
//
//  Created by Vladyslav Bedro on 8/15/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//
//

#import "Holiday+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface Holiday (CoreDataProperties)

+ (NSFetchRequest<Holiday *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *date;
@property (nullable, nonatomic, copy) NSString *name;

@end

NS_ASSUME_NONNULL_END
